import { RequestConfig } from 'umi';

export const layout = () => {
  return {
      rightContentRender: () => <div>rightContentRender</div>,
      footerRender: () => <div>footer</div>,
      onPageChange: () => {
        console.log('onPageChange')
      },
      menuHeaderRender: undefined,
  }
}

export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [],
  responseInterceptors: [],
};