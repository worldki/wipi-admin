import styles from './index.less';
import { login } from "@/services/user";
import { Button } from "antd";
import { useHistory } from 'umi';
export default function IndexPage() {
  const history = useHistory();
  const handleLogin = () => {
    login().then(res => {
      localStorage.setItem('userInfo',JSON.stringify(res.data));
      history.push('/');
    })
  }
  return (
    <div>
      <h1 className={styles.title}>login index</h1>
      <Button onClick={handleLogin}>登录</Button>
    </div>
  );
}
